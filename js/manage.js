var instances;
$(document).ready(function() {
  var elems = document.getElementById('modal1');
  var options = {};
  instances = M.Modal.init(elems, options);
  instances.open();
  displayDevices();
});

function displayDevices() {
  var myObj = {};
  myObj["userId"] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'viewdevices/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuth,
      "sessionKey": sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      usingInfo = usingInfo.Data;
      var tableView = '';
      $.each(usingInfo, function(index, value) {
        tableView = tableView + '<tr class="hoverable"><td>' + value.deviceName + '</td><td>' + value.deviceType + '</td><td>' + value.emailAddress + '</td><td>' + value.deviceIdentifier + '</td><td><a href="javascript:displayTracking(' + value.deviceId + ')">Details</a></td></tr>'
      })
      $("#tableContainer").append(tableView);
    },
    error: function(response) {
      M.toast({
        html: 'Somthing is not right here please try again later!'
      });
      var usingInfo = JSON.parse(JSON.stringify(response));
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displayTracking(deviceId) {
  console.log(deviceId);
  var myObj = {};
  myObj["userId"] = sessionInfo.sessionInfo.userId;
  myObj["deviceId"] = deviceId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'viewtracking/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuth,
      "sessionKey": sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      usingInfo = usingInfo.Data;
      var tableView = '';
      $.each(usingInfo, function(index, value) {
        tableView = tableView + '<tr class="hoverable"><td>' + value.tackingValue + '</td><td>' + value.DateTimeField + '</td></tr>'
      })

      document.getElementById("modalTable").innerHTML = tableView;
      instances.open();
      $("#modal1").css("display", "block");
    },
    error: function(response) {
      M.toast({
        html: 'Somthing is not right here please try again later!'
      });
      var usingInfo = JSON.parse(JSON.stringify(response));
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function closeModal() {
  instances.close();
}
