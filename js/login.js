$(document).ready(function() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        location.replace("index.php");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        location.replace("index.php");
      }
    }
  }
});

function loginUser() {
  event.preventDefault();
  var userName = document.getElementById("icon_prefix").value;
  var password = document.getElementById("password-field").value;
  var rememberMe = document.getElementById("rememberMe").checked;
  if (userName != "") {
    if (password != "") {
      var myObj = {};
      myObj["userName"] = userName;
      myObj["password"] = password;
      var jsonObj = JSON.stringify(myObj);
      $.ajax({
        url: beUrl() + 'login/',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: jsonObj,
        success: function(response) {
          if (rememberMe == true) {
            localStorage.setItem("adminSessionInfo", JSON.stringify(response));
            localStorage.setItem("rememberMe", 1);
            location.replace("index.php");
          } else {
            sessionStorage.setItem("adminSessionInfo", JSON.stringify(response));
            localStorage.setItem("rememberMe", 0);
            location.replace("index.php");
          }
        },
        error: function(response) {
          M.toast({
            html: 'Invalid username or password passed'
          });
        }
      });
    } else {
      M.toast({
        html: 'Password cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}
