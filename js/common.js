var sessionInfo;
var logOut = '<li class="bold"><a href="javascript:logoutModule()" class="waves-effect waves-teal inLineText"><img src="img/newicon/logout.png" class="menuManagerIcon" /><span class="fontSize">Logout</span></a></li>';
var homeManagement = '<li class="bold"><a href="index.php" class="waves-effect waves-teal inLineText"><img src="img/newicon/home.png" class="menuManagerIcon" /><span class="fontSize">Home</span></a></li>';
var display = {
  "log": 1,
  "home": 1
}
$(document).ready(function() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("loginuser.php");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("loginuser.php");
      }
    }
  } else {
    location.replace("loginuser.php");
  }
  menuManager();
});

function menuManager() {
  var menuPrint = ''
  if (display.log == 1) {
    menuPrint = menuPrint + logOut;
  }

  menuPrint = homeManagement + menuPrint;
  $("#nav-mobile").append(menuPrint);
}

function logoutModule() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      localStorage.removeItem("adminSessionInfo");
    } else {
      sessionStorage.removeItem("adminSessionInfo");
    }
    localStorage.removeItem("rememberMe");
  } else {
    localStorage.removeItem("adminSessionInfo");
    sessionStorage.removeItem("adminSessionInfo");
  }
  //location.replace("loginuser.php")
  logoutFinal();
}

function logoutFinal() {
  var myObj = {};
  myObj["userId"] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'logout/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuth,
      "sessionKey":sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      location.replace("loginuser.php")
    },
    error: function(response) {
      location.replace("loginuser.php")
    }
  });
}
